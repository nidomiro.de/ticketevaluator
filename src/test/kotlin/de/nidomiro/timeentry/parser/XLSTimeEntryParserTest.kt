package de.nidomiro.timeentry.parser

import assertk.assertThat
import assertk.assertions.containsOnly
import assertk.assertions.isEqualTo
import assertk.assertions.isSuccess
import de.nidomiro.assertThat
import de.nidomiro.domain.TimeEntry
import org.junit.jupiter.api.Test
import java.time.Duration
import java.time.LocalDate
import java.time.LocalTime

internal class XLSTimeEntryParserTest {

    @Test
    fun parseEntries() {

        val fileInputStream = this.javaClass.getResourceAsStream("/examples/002_simple_time_entries.xls")
        val parser: TimeEntryParser =
            XLSTimeEntryParser(fileInputStream)
        val result = parser.parseEntries()

        assertThat(result.size).isEqualTo(61)

        assertThat("Values test: Row 1 (3)") { result[1] }.isSuccess()
            .isEqualTo(
                TimeEntry(
                    LocalDate.of(2019, 5, 2),
                    Duration.ofHours(6),
                    LocalTime.of(8, 30),
                    LocalTime.of(14, 30),
                    "AGILE",
                    true,
                    1.0,
                    6.0,
                    "PersonA",
                    ""
                )
            )

        assertThat("Values test: Row 9 (11)") { result[9] }.isSuccess()
            .isEqualTo(
                TimeEntry(
                    LocalDate.of(2019, 5, 6),
                    Duration.ofMinutes(54),
                    LocalTime.of(12, 57),
                    LocalTime.of(13, 51),
                    "ABC",
                    true,
                    2.0,
                    1.8,
                    "PersonB",
                    "ABC-1"
                )
            )


        assertThat("Exactly three distinct people") { result.distinctBy { it.name } }
            .isSuccess()
            .transform { it.size }
            .isEqualTo(3)

        assertThat("Exactly five distinct descriptions") {
            result.distinctBy { it.description }.filter { it.description.isNotEmpty() }
        }
            .isSuccess()
            .apply {
                transform { it.size }.isEqualTo(5)
                transform { list -> list.map { it.description } }.containsOnly(
                    "ABC-1",
                    "ABC-2",
                    "ABC-3",
                    "ABC-4",
                    " Space Test "
                )
            }

    }

    @Test
    fun `parseEntries does trim some spaces but not all`() {

        val fileInputStream = this.javaClass.getResourceAsStream("/examples/002_simple_time_entries.xls")
        val parser: TimeEntryParser =
            XLSTimeEntryParser(fileInputStream)
        val result = parser.parseEntries()

        assertThat { result[60] }.isSuccess()
            .isEqualTo(
                TimeEntry(
                    LocalDate.of(2019, 5, 15),
                    Duration.ofMinutes(15),
                    LocalTime.of(14, 30),
                    LocalTime.of(14, 45),
                    "SONST",
                    false,
                    null,
                    null,
                    "PersonC",
                    " Space Test "
                )
            )

    }
}