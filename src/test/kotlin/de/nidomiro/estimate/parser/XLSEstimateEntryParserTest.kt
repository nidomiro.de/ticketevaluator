package de.nidomiro.estimate.parser

import assertk.assertThat
import assertk.assertions.isEqualTo
import de.nidomiro.domain.EstimateAction
import de.nidomiro.domain.RawEstimateEntry
import org.junit.jupiter.api.Test
import java.time.LocalDate

internal class XLSEstimateEntryParserTest {

    @Test
    fun parseEntries() {

        val fileInputStream = this.javaClass.getResourceAsStream("/examples/001_parse_test_estimates.xls")
        val parser: EstimateEntryParser =
            XLSEstimateEntryParser(fileInputStream)
        val result = parser.parseEntries()

        assertThat(result.size).isEqualTo(6)
        assertThat(result[0]).isEqualTo(
            RawEstimateEntry(
                "ABC-1",
                0.25,
                LocalDate.of(2019, 5, 14),
                "1",
                EstimateAction.SET,
                "Initial Estimate"
            )
        )
        assertThat(result[1]).isEqualTo(
            RawEstimateEntry(
                "ABC-2",
                1.25,
                LocalDate.of(2019, 5, 14),
                "1",
                EstimateAction.SET,
                "Initial Estimate"
            )
        )
        assertThat(result[2]).isEqualTo(
            RawEstimateEntry(
                "ABC-1",
                null,
                LocalDate.of(2019, 5, 14),
                "1",
                EstimateAction.CUT,
                "Cut all time entries before this date"
            )
        )
        assertThat(result[3]).isEqualTo(
            RawEstimateEntry(
                "ABC-2",
                null,
                LocalDate.of(2019, 5, 14),
                "1",
                EstimateAction.CUT,
                "Cut all time entries before this date"
            )
        )
        assertThat(result[4]).isEqualTo(
            RawEstimateEntry(
                "ABC-1",
                2.0,
                LocalDate.of(2019, 5, 24),
                "2",
                EstimateAction.SET,
                "Updated Estimate for sprint 2"
            )
        )
        assertThat(result[5]).isEqualTo(
            RawEstimateEntry(
                "ABC-3",
                0.5,
                LocalDate.of(2019, 5, 24),
                "2",
                EstimateAction.SET,
                " Spaces test "
            )
        )

    }
}