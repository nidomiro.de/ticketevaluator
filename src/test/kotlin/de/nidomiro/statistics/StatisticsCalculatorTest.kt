package de.nidomiro.statistics

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import assertk.assertions.isSuccess
import de.nidomiro.domain.StatisticsContext
import de.nidomiro.domain.TicketEffortDto
import de.nidomiro.domain.TimeEntry
import org.junit.jupiter.api.Test
import java.time.Duration
import java.time.LocalDate
import java.time.LocalTime

internal class StatisticsCalculatorTest {

    @Test
    fun doesAddSameTickets() {

        val statisticsCalculator = StatisticsCalculator()

        val context = StatisticsContext(
            timeEntries = listOf(
                TimeEntry(
                    LocalDate.of(2019, 5, 15),
                    Duration.ofMinutes(15),
                    LocalTime.of(14, 30),
                    LocalTime.of(14, 45),
                    "ABC",
                    false,
                    null,
                    null,
                    "PersonC",
                    "ABC-1:sakjhsdkfjh"
                ),
                TimeEntry(
                    LocalDate.of(2019, 5, 16),
                    Duration.ofMinutes(15),
                    LocalTime.of(14, 30),
                    LocalTime.of(14, 45),
                    "CBA",
                    false,
                    null,
                    null,
                    "PersonC",
                    "ABC-1sadfsdjfkj"
                )
            )
        )

        assertThat { statisticsCalculator.calculateEffortPerTicket(context) }
            .isSuccess()
            .transform { it.ticketEfforts?.getOrNull(0) }
            .isNotNull()
            .isEqualTo(
                TicketEffortDto(
                    ticket = "ABC-1",
                    effort = (15.0 + 15.0) / 60 / 8,
                    latestLoggedTime = LocalDate.of(2019, 5, 16)
                )
            )


    }
}