package de.nidomiro.runner

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.io.InputStream
import java.io.OutputStream

@Disabled("Have to be Rewritten")
class RunnerTest {

    @Test
    fun run() {

        val args = object : RunnerArguments {
            override val timeEntryFile: InputStream
                get() = this.javaClass.getResourceAsStream("/timesheet-to-evaluate.xls")
            override val outputStream: OutputStream?
                get() = null
            override val estimateFile: InputStream?
                get() = this.javaClass.getResourceAsStream("/estimates.xls")
            override val ticketFactor: Double
                get() = 2.0
            override val displayWeeksBackLimit: Long?
                get() = null

        }


        val runner = Runner()
        runner.run(args)

    }
}