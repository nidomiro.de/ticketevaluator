@file:Suppress("DEPRECATION")

package de.nidomiro

import assertk.Assert
import assertk.Result


/**
 * Asserts on the given block returning an `Assert<Result<T>>`. You can test that it returns a value or throws an exception.
 *
 * ```
 * assertThat("myCustomName") { 1 + 1 }.isSuccess().isPositive()
 *
 * assertThat("myCustomName") {
 *   throw Exception("error")
 * }.isFailure().hasMessage("error")
 * ```
 */
@Suppress("DEPRECATION")
inline fun <T> assertThat(name: String, f: () -> T): Assert<Result<T>> = assertk.assertThat(Result.runCatching(f), name)

