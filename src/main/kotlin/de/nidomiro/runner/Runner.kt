package de.nidomiro.runner

import de.nidomiro.domain.StatisticsContext
import de.nidomiro.estimate.parser.XLSEstimateEntryParser
import de.nidomiro.output.AsciiTablePrinter
import de.nidomiro.output.Printer
import de.nidomiro.statistics.DifferenceCalculator
import de.nidomiro.statistics.StatisticsCalculator
import de.nidomiro.timeentry.parser.TimeEntryParser
import de.nidomiro.timeentry.parser.XLSTimeEntryParser
import java.time.LocalDate

class Runner {

    fun run(args: RunnerArguments) {

        val timeEntryParser: TimeEntryParser = XLSTimeEntryParser(args.timeEntryFile)
        val estimateParser = args.estimateFile?.let { XLSEstimateEntryParser(it) }
        val calculator = StatisticsCalculator()
        val differenceCalculator = DifferenceCalculator()

        var context = StatisticsContext(
            timeEntries = timeEntryParser.parseEntries(),
            rawEstimateEntries = estimateParser?.parseEntries() ?: listOf()
        )

        context = calculator.calculateEffortPerTicket(context, args.ticketFactor)
        context = differenceCalculator.calcDifference(context)

        val printer: List<Printer> = listOf(
            AsciiTablePrinter()
        )

        val afterValue = args.displayWeeksBackLimit?.let { LocalDate.now().minusWeeks(it) } ?: LocalDate.MIN

        printer.forEach { it.print(context, afterValue) }
    }


}