package de.nidomiro.runner

import java.io.InputStream
import java.io.OutputStream

interface RunnerArguments {

    val timeEntryFile: InputStream

    val outputStream: OutputStream?

    val estimateFile: InputStream?

    val ticketFactor: Double

    val displayWeeksBackLimit: Long?

}