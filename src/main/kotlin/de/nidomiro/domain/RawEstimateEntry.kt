package de.nidomiro.domain

import java.time.LocalDate

data class RawEstimateEntry(
    val ticket: String,
    val estimate: Double?,
    val date: LocalDate,
    val sprint: String?,
    val action: EstimateAction,
    val note: String?
)