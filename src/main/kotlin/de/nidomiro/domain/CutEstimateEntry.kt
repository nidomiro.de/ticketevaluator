package de.nidomiro.domain

import java.time.LocalDate

data class CutEstimateEntry(
    val ticket: String,
    val date: LocalDate,
    val sprint: String?,
    val note: String?
)