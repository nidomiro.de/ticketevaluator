package de.nidomiro.domain

import java.time.LocalDate

data class SetEstimateEntry(
    val ticket: String,
    val estimate: Double,
    val date: LocalDate,
    val sprint: String?,
    val note: String?
)