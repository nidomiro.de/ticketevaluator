package de.nidomiro.domain

import java.time.LocalDate

data class TicketEffortEstimateDifferenceDto(
    val ticket: String,
    val effort: Double,
    val effortWithFactor: Double?,
    val estimateWithFactor: Double?,
    val differenceWithFactor: Double?,

    val latestLoggedTime: LocalDate
)