package de.nidomiro.domain

data class StatisticsContext(

    val timeEntries: List<TimeEntry> = listOf(),
    val rawEstimateEntries: List<RawEstimateEntry> = listOf(),
    /**
     * key = Ticket
     */
    val setEstimateEntries: Map<String, List<SetEstimateEntry>> = mapOf(),
    /**
     * key = Ticket
     */
    val cutEstimateEntries: Map<String, List<CutEstimateEntry>> = mapOf(),

    val ticketEfforts: List<TicketEffortDto>? = null,

    val ticketEffortDifferences: List<TicketEffortEstimateDifferenceDto>? = null


)