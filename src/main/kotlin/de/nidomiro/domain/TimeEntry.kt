package de.nidomiro.domain

import java.time.Duration
import java.time.LocalDate
import java.time.LocalTime

data class TimeEntry(
    val date: LocalDate,
    val duration: Duration,
    val startTime: LocalTime?,
    val endTime: LocalTime?,
    val activity: String,
    val billable: Boolean,
    val hourlyRate: Double?,
    val moneyValue: Double?,
    val name: String,
    val description: String
)