package de.nidomiro.domain

import java.time.LocalDate

data class TicketEffortDto(
    val ticket: String,
    val effort: Double,
    val effortWithFactor: Double? = null,

    val latestLoggedTime: LocalDate
)