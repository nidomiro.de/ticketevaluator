package de.nidomiro.domain

enum class EstimateAction {
    SET,
    CUT
}