package de.nidomiro.util

import kotlin.math.pow
import kotlin.math.round

fun Double.format(digits: Int): String {

    val roundingFactor = 10.0.pow(digits.toDouble())

    var rounded = round(this * roundingFactor) / roundingFactor

    if (rounded == -0.0) {
        rounded = 0.0
    }

    return java.lang.String.format("%.${digits}f", rounded)
}