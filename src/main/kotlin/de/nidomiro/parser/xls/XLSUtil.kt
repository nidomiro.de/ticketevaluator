package de.nidomiro.parser.xls

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.DataFormatter


val Cell?.nullIfNoValue: Cell?
    get() {
        return this?.let {
            when {
                it.cellType == CellType.BLANK -> null
                it.cellType == CellType.STRING
                        && it.stringCellValue.isBlank() -> null
                else -> it
            }
        }
    }


val Cell.asStringValue: String
    get() {
        val objDefaultFormat = DataFormatter()
        return objDefaultFormat.formatCellValue(this)
    }

val Cell.numericCellValueOrNull: Double?
    get() = when (this.cellType) {
        CellType.BLANK -> null
        else -> this.numericCellValue
    }
