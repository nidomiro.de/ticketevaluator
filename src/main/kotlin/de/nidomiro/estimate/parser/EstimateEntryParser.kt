package de.nidomiro.estimate.parser

import de.nidomiro.domain.RawEstimateEntry

interface EstimateEntryParser {
    fun parseEntries(): List<RawEstimateEntry>
}