package de.nidomiro.estimate.parser

import de.nidomiro.domain.EstimateAction
import de.nidomiro.domain.RawEstimateEntry
import de.nidomiro.parser.xls.asStringValue
import de.nidomiro.parser.xls.nullIfNoValue
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.WorkbookFactory
import java.io.InputStream
import java.time.ZoneId

class XLSEstimateEntryParser(
    private val inputStream: InputStream
) : EstimateEntryParser {


    override fun parseEntries(): List<RawEstimateEntry> {
        val workbook = WorkbookFactory.create(inputStream)
        val timeSheet = workbook.getSheetAt(0)

        val list = mutableListOf<RawEstimateEntry>()
        for (i in (timeSheet.firstRowNum + 1)..timeSheet.lastRowNum) {
            val entry = timeSheet.getRow(i)?.let { rowToEntry(it) }
            entry?.let { list.add(it) }
        }

        return list
    }

    private fun rowToEntry(row: Row): RawEstimateEntry? { // TODO: ErrorHandling

        val rawTicket = row.getCell(0).nullIfNoValue?.stringCellValue?.trim()
        val rawEstimate = row.getCell(1).nullIfNoValue?.numericCellValue
        val rawDate = row.getCell(2).nullIfNoValue?.dateCellValue
        val rawSprint = row.getCell(3).nullIfNoValue?.asStringValue?.trim()
        val rawAction = row.getCell(4).nullIfNoValue?.stringCellValue?.trim()
        val rawNote = row.getCell(5).nullIfNoValue?.stringCellValue

        val actionEnum = rawAction?.let { EstimateAction.valueOf(rawAction) } ?: EstimateAction.SET

        return if (rawTicket != null && rawDate != null) {
            RawEstimateEntry(
                ticket = rawTicket,
                estimate = rawEstimate,
                date = rawDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                sprint = rawSprint,
                action = actionEnum,
                note = rawNote

            )
        } else {
            println("Could not Parse row: ${row.rowNum+1}")
            null
        }
    }
}


