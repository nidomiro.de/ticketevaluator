package de.nidomiro.statistics

import de.nidomiro.domain.StatisticsContext
import de.nidomiro.domain.TicketEffortEstimateDifferenceDto
import java.time.LocalDate

class DifferenceCalculator {

    fun calcDifference(context: StatisticsContext): StatisticsContext {

        val ticketsWithEstimateAndNoTimeEntry =
            (context.setEstimateEntries.map { it.key } - context.cutEstimateEntries.map { it.key }).toMutableList()

        val differences = context.ticketEfforts?.map { effort ->

            ticketsWithEstimateAndNoTimeEntry.remove(effort.ticket)

            val estimate = context.setEstimateEntries[effort.ticket]?.first()
            TicketEffortEstimateDifferenceDto(
                ticket = effort.ticket,
                effort = effort.effort,
                effortWithFactor = effort.effortWithFactor,
                estimateWithFactor = estimate?.estimate,
                differenceWithFactor = if (estimate?.estimate != null && effort.effortWithFactor != null) {
                    estimate.estimate - effort.effortWithFactor
                } else {
                    null
                },
                latestLoggedTime = effort.latestLoggedTime
            )
        }


        val ticketsInSprintWithoutTimeEntry = ticketsWithEstimateAndNoTimeEntry.map {
            val setEntry = context.setEstimateEntries[it]
                ?.maxBy { entry -> entry.date }!!

            TicketEffortEstimateDifferenceDto(
                ticket = it,
                effort = 0.0,
                effortWithFactor = 0.0,
                estimateWithFactor = setEntry.estimate,
                differenceWithFactor = setEntry.estimate,
                latestLoggedTime = LocalDate.now()
            )
        }


        val result = differences
            ?.filter { it.effort >= 0.01 }
            ?.plus(ticketsInSprintWithoutTimeEntry)
            ?.sortedBy { it.ticket }


        return context.copy(
            ticketEffortDifferences = result
        )
    }
}