package de.nidomiro.statistics

import de.nidomiro.domain.*
import java.time.LocalDate

class StatisticsCalculator {


    fun calculateEffortPerTicket(
        context: StatisticsContext,
        ticketFactor: Double? = null,
        regexString: String = """^[a-zA-Z0-9]+-\d+"""
    ): StatisticsContext {

        val preprocessedContext = preprocessContext(context)


        val regex = regexString.toRegex(RegexOption.IGNORE_CASE)

        val result = preprocessedContext.timeEntries
            .groupBy { timeEntry ->
                regex.find(timeEntry.description)?.value
                    ?: timeEntry.activity
            }
            .asSequence()
            .map {

                val trimmedTimeEntries = it.value.filter { timeEntry ->
                    timeEntry.date.isAfter(
                        preprocessedContext.cutEstimateEntries[it.key]?.first()?.date ?: LocalDate.MIN
                    )
                }

                val durationSum = trimmedTimeEntries
                    .sumBy { tickets -> tickets.duration.toMinutes().toInt() }
                    .toPTFromMinutes()
                val durationSumWithFactor = ticketFactor?.let { durationSum * ticketFactor }
                val lastLoggedTime = trimmedTimeEntries.maxBy { tickets -> tickets.date }?.date ?: LocalDate.now()

                TicketEffortDto(
                    ticket = it.key,
                    effort = durationSum,
                    effortWithFactor = durationSumWithFactor,
                    latestLoggedTime = lastLoggedTime
                )
            }
            .sortedBy { it.ticket }


        return preprocessedContext.copy(ticketEfforts = result.toList())
    }

    private fun Int.toPTFromMinutes() = this.toDouble() / 60 / 8

    private fun preprocessContext(context: StatisticsContext): StatisticsContext {
        val grouped = context.rawEstimateEntries.groupBy { it.action }

        val setEstimates = grouped[EstimateAction.SET]
            ?.mapNotNull {
                if (it.estimate != null) {
                    SetEstimateEntry(
                        ticket = it.ticket,
                        estimate = it.estimate,
                        date = it.date,
                        sprint = it.sprint,
                        note = it.note
                    )
                } else {
                    null
                }
            }
            ?.groupBy { it.ticket }
            ?.mapValues { it.value.sortedByDescending { entry -> entry.date } }

        val cutEstimates = grouped[EstimateAction.CUT]
            ?.map {
                CutEstimateEntry(
                    ticket = it.ticket,
                    date = it.date,
                    sprint = it.sprint,
                    note = it.note
                )
            }
            ?.groupBy { it.ticket }
            ?.mapValues { it.value.sortedByDescending { entry -> entry.date } }

        return context.copy(
            setEstimateEntries = setEstimates ?: mapOf(),
            cutEstimateEntries = cutEstimates ?: mapOf()
        )
    }

}