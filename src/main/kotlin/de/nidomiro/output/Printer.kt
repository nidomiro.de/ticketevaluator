package de.nidomiro.output

import de.nidomiro.domain.StatisticsContext
import java.time.LocalDate

interface Printer {

    fun print(context: StatisticsContext, after: LocalDate = LocalDate.MIN)
}