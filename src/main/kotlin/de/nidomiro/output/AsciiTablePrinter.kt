package de.nidomiro.output

import de.nidomiro.domain.StatisticsContext
import de.nidomiro.domain.TicketEffortEstimateDifferenceDto
import de.nidomiro.util.format
import de.vandermeer.asciitable.AsciiTable
import de.vandermeer.asciitable.CWC_LongestLine
import de.vandermeer.asciithemes.u8.U8_Grids
import java.time.LocalDate

class AsciiTablePrinter : Printer {

    private val asciiTable = AsciiTable()

    init {
        asciiTable.apply {
            addRule()
            addRow("Ticket", "Real", "Real with factor", "Estimate", "Time left")
            addRule()
            addRule()
        }
    }


    override fun print(context: StatisticsContext, after: LocalDate) {

        context.ticketEffortDifferences
            ?.filter { it.latestLoggedTime.isAfter(after) }
            ?.forEach {
                asciiTable.addRow(
                    it.ticket,
                    it.effort.format(2),
                    it.effortWithFactor?.format(2) ?: " - ",
                    it.estimateWithFactor?.format(2) ?: " - ",
                    calculateDifferenceStringForTable(it)
                )
                asciiTable.addRule()
            }

        asciiTable.apply {
            this@apply.context.grid = U8_Grids.borderDoubleLight()
            renderer.cwc = CWC_LongestLine()

            setPaddingLeft(1)
            setPaddingRight(1)
        }

        println(asciiTable.render())
    }

    private fun calculateDifferenceStringForTable(row: TicketEffortEstimateDifferenceDto): String = when {
        row.differenceWithFactor != null -> {
            val formattedNumber = row.differenceWithFactor.format(2)
            when {
                row.differenceWithFactor <= -0.005 -> "$formattedNumber !!!"
                row.effort == 0.0 -> "$formattedNumber (no time entry)"
                else -> formattedNumber
            }
        }
        row.effort > 0 -> " ? (no estimate)"
        else -> " - "

    }


}