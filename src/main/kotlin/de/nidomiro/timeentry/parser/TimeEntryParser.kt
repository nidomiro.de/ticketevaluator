package de.nidomiro.timeentry.parser

import de.nidomiro.domain.TimeEntry

interface TimeEntryParser {
    fun parseEntries(): List<TimeEntry>
}