package de.nidomiro.timeentry.parser

import de.nidomiro.domain.TimeEntry
import de.nidomiro.parser.xls.asStringValue
import de.nidomiro.parser.xls.numericCellValueOrNull
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import java.io.InputStream
import java.time.Duration
import java.time.LocalTime
import java.time.ZoneId

class XLSTimeEntryParser(
    private val inputStream: InputStream
) : TimeEntryParser {


    override fun parseEntries(): List<TimeEntry> {
        val workbook = WorkbookFactory.create(inputStream)
        val timeSheets = getSheets(workbook)

        return timeSheets.flatMap { timeSheet ->
            parseSheet(timeSheet)
        }
    }

    private fun getSheets(workbook: Workbook): List<Sheet> {
        val lastSheetIndex = workbook.numberOfSheets - 1

        return when {
            lastSheetIndex < 0 -> listOf()
            lastSheetIndex == 0 -> listOf(workbook.getSheetAt(0))
            else -> (1..lastSheetIndex).mapNotNull {
                workbook.getSheetAt(it)
            }
        }
    }

    private fun parseSheet(timeSheet: Sheet): MutableList<TimeEntry> {
        val list = mutableListOf<TimeEntry>()

        val rowMapping = determineRowMapping(timeSheet.getRow(timeSheet.firstRowNum))

        for (i in (timeSheet.firstRowNum + 1)..timeSheet.lastRowNum) {
            val row = timeSheet.getRow(i)
            val timeEntry = row?.let { rowToTimeEntry(it, rowMapping) }
            timeEntry?.let { list.add(it) }
        }

        return list
    }

    private fun rowToTimeEntry(row: Row, rowMappings: Map<CellHeadlines, Int>): TimeEntry? =
        if (row.lastCellNum >= rowMappings.size) {

            fun Row.getCell(headline: CellHeadlines) = getCell(rowMappings.getValue(headline))


            TimeEntry(
                date = row.getCell(CellHeadlines.Date).dateCellValue.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                duration = Duration.between(
                    LocalTime.MIN,
                    row.getCell(CellHeadlines.Duration).dateCellValue.toInstant().atZone(ZoneId.systemDefault()).toLocalTime()
                ),
                startTime = row.getCell(CellHeadlines.StartTime).dateCellValue?.toInstant()?.atZone(
                    ZoneId.systemDefault()
                )?.toLocalTime(),
                endTime = row.getCell(CellHeadlines.EndTime).dateCellValue?.toInstant()?.atZone(
                    ZoneId.systemDefault()
                )?.toLocalTime(),
                activity = row.getCell(CellHeadlines.Activity).stringCellValue.trim(),
                billable = row.getCell(CellHeadlines.Billable).booleanCellValue,
                hourlyRate = row.getCell(CellHeadlines.HourlyRate).numericCellValueOrNull,
                moneyValue = row.getCell(CellHeadlines.MoneyValue).numericCellValueOrNull,
                name = row.getCell(CellHeadlines.PersonName).stringCellValue.trim(),
                description = row.getCell(CellHeadlines.Description).stringCellValue
            )
        } else {
            null
        }

    private fun determineRowMapping(row: Row): Map<CellHeadlines, Int> {
        val rowMapping = mutableMapOf<CellHeadlines, Int>()

        for (i in row.firstCellNum until row.lastCellNum) {
            for (headline in CellHeadlines.values()) {
                if (headline.titleValue == row.getCell(i).asStringValue.trim()) {
                    rowMapping[headline] = i
                    break
                }
            }
        }

        return rowMapping
    }

}

enum class CellHeadlines(val titleValue: String) {
    Date("Datum"),
    Duration("Dauer"),
    StartTime("Startzeit"),
    EndTime("Endzeit"),
    Activity("Aktivität"),
    Billable("Fakturierbar"),
    HourlyRate("Stundensatz"),
    MoneyValue("Betrag"),
    PersonName("Name"),
    Description("Beschreibung"),

}


