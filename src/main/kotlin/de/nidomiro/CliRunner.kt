package de.nidomiro

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import com.xenomachina.argparser.mainBody
import de.nidomiro.runner.Runner
import de.nidomiro.runner.RunnerArguments
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream


fun main(rawArgs: Array<String>) = mainBody {

    val argParser = ArgParser(rawArgs)
    val args = argParser.parseInto(::MyArgs)

    val runner = Runner()
    runner.run(args)
}


class MyArgs(parser: ArgParser) : RunnerArguments {

    val timeEntryFilePath by parser.storing(
        "-i", "--input",
        help = "The path to the input-file"
    )

    val outputFilePath by parser.storing(
        "-o", "--output",
        help = "The path to the output-file"
    )
        .default<String?>(null)

    val estimateFilePath by parser.storing(
        "-e", "--estimate-input",
        help = "The path to the input-file containing the estimations for the Tickets"
    )
        .default<String?> { null }

    override val ticketFactor by parser.storing(
        "-f", "--ticketFactor",
        help = "The factor to multiply the parsed time with"
    )
    { toDouble() }
        .default<Double>(1.0)

    override val displayWeeksBackLimit by parser.storing(
        "-l", "--weeksBackLimit",
        help = "Removes all entries where the last entry happened longer than x weeks ago"
    )
    { toLong() }
        .default<Long?>(null)


    override val timeEntryFile: InputStream
        get() = FileInputStream(timeEntryFilePath)
    override val outputStream: OutputStream?
        get() = outputFilePath?.let { FileOutputStream(it) }
    override val estimateFile: InputStream?
        get() = estimateFilePath?.let { FileInputStream(it) }


}

